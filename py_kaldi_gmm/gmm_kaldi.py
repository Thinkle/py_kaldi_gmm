#%%
import numpy as np
import os
from pathlib import Path
import time
import shutil
from joblib import Parallel, delayed
import tempfile 
import sys
from .utils import save_to_kaldi_feature_format, run_command

# 加入二进制路径
sys.path.append(
    os.path.abspath(
        os.path.join(
            __file__,
            '..',
            'bin'
        )
    )
)

def fetch_averaged_scores(like_files):
    names = []
    scores = []
    with open(like_files) as f:
        for line in f:
            wav_name, score = line.split()
            names.append(wav_name)
            scores.append(float(score))
    return names, scores


#%%
class DiagonalGMM:
    def __init__(self, save_model, num_gauss=512, num_gauss_init_ratio=0.5, min_variance=0.001, temp_dir=None):
        self.num_gauss = num_gauss
        self.num_gauss_init_ratio = num_gauss_init_ratio
        self.min_variance = min_variance
        self.save_model = save_model
        self.is_init = os.path.exists(save_model)
        self.model_bin = None
        self.remove_tempdir_when_del = True
        if temp_dir:
            self.temp_dir = temp_dir 
        else:
            self.temp_dir = tempfile.mkdtemp()

        
        # random_str = f'{time.time()}-{np.random.rand()}'
        # self.temp_dir = os.path.join(temp_dir, random_str) if temp_dir else f'/tmp/chengxingliang/DiagonalGMM/{random_str}'
        Path(self.temp_dir).mkdir(parents=True, exist_ok=True)

    def __del__(self):
        if self.remove_tempdir_when_del and os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir)

    def _read_model_from_file(self, file):
        with open(file, 'rb') as f:
            self.model_bin = f.read()

    def _write_model_to_file(self, file):
        assert(self.model_bin is not None)
        with open(file, 'wb') as f:
            f.write(self.model_bin)

    def _init(self, feat_ark, verbose=0):
        run_command([
            'gmm-global-init-from-feats',
            f'--num-gauss={self.num_gauss}',
            f'--num-gauss-init={int(self.num_gauss_init_ratio*self.num_gauss)}',
            f'--min_variance={self.min_variance}',
            f'ark:{feat_ark}',
            self.save_model,
        ], print_stdout=(verbose != 0))
        self._read_model_from_file(self.save_model)
        self.is_init = True
        return self

    def _preprocess_feature(self, X, split=None):
        if type(X) == str:
            assert(split == None)
            return X

        if type(X) in [list, np.ndarray]:
            if type(X) == list:
                assert(len(X[0].shape) == 2)
            elif type(X) == np.ndarray:
                assert(len(X.shape) == 3)

            save_feat_file = self.temp_dir + f'/feat-{time.time()}.ark'
            Path(self.temp_dir).mkdir(parents=True, exist_ok=True)
            if split is None:
                save_to_kaldi_feature_format(save_feat_file, X)
                return save_feat_file
            else:
                n_per_split = int(np.ceil(len(X)/split))
                all_save_names = []
                for i in range(split):
                    split_file_name = f'{save_feat_file}.split.{i+1:02d}'
                    save_to_kaldi_feature_format(
                        save_file=split_file_name,
                        feat_mats=X[i*n_per_split:(i+1)*n_per_split],
                    )
                    all_save_names.append(split_file_name)
                return all_save_names
        
        raise RuntimeError(F"Unknow X type {type(X)}")

    def fit(self, X, y=None, n_iter=10, n_jobs=4, verbose=0):
        """ 
        Parameter
            X: array, shape (n_samples, feat_shpae1, feat_shape2), or string
                输入的特征矩阵(一个array)
                或者已经保存好，kaldi形式的ark特征文件 （path)
            
            y: not used
            n_iter: 训练迭代次数
            force_init: 是否强制初始化。如果不强制初始化，当已经存在模型时，会继续训练
        """
        Path(self.temp_dir).mkdir(parents=True, exist_ok=True)
        feat_ark = self._preprocess_feature(X)
        self._init(feat_ark, verbose=verbose)

        n_jobs = max(1, min(n_jobs, int(len(X)/10)))
        feat_ark_list = self._preprocess_feature(X, split=n_jobs)
        
        TEMP_ACC_FILE = self.temp_dir + '/tmp.acc'
        for _ in range(n_iter):
            Parallel(n_jobs=n_jobs)([
                delayed(run_command)([
                    'gmm-global-acc-stats',
                    self.save_model,
                    f'ark:{feat_ark_list[i_job]}',
                    TEMP_ACC_FILE+f'.split.{i_job+1}',
                ], print_stdout=(verbose != 0))
                for i_job in range(n_jobs)
            ])
            run_command([
                'gmm-global-est',
                f'--min_variance={self.min_variance}',
                self.save_model,
                # TEMP_ACC_FILE+'.split.1',
                f"gmm-global-sum-accs - {TEMP_ACC_FILE}.split.*|",
                self.save_model,
            ], print_stdout=(verbose != 0))

        self._read_model_from_file(self.save_model)
        return self

    def __fit(self, X, y=None, n_iter=10, verbose=0):
        """ 
        单线程版本（废弃，调试备用）
        Parameter
            X: array, shape (n_samples, n_feature), or string
                输入的特征矩阵(一个array)
                或者已经保存好，kaldi形式的ark特征文件
            
            y: not used
            n_iter: 训练迭代次数
            force_init: 是否强制初始化。如果不强制初始化，当已经存在模型时，会继续训练
        """
        feat_txt = self._preprocess_feature(X)

        self._init(feat_txt, verbose=verbose)

        TEMP_ACC_FILE = self.temp_dir + '/tmp.acc'
        for _ in range(n_iter):
            run_command([
                'gmm-global-acc-stats',
                self.save_model,
                f'ark:{feat_txt}',
                TEMP_ACC_FILE,
            ], print_stdout=(verbose != 0))
            #    gmm-est --write-occs=$dir/$[$x+1].occs --mix-up=$numgauss --power=$power $dir/$x.mdl  "gmm-sum-accs - $dir/$x.*.acc|" $dir/$[$x+1].mdl || exit 1;
            run_command([
                'gmm-global-est',
                f'--min_variance={self.min_variance}',
                self.save_model,
                TEMP_ACC_FILE,
                self.save_model,
            ], print_stdout=(verbose != 0))

        self._read_model_from_file(self.save_model)
        return self

    def adapt_map(self, X, y=None, tau=10, verbose=0):
        """ 
        Parameter
            X: array, shape (n_samples, n_feature), or string
                输入的特征矩阵(一个array)
                或者已经保存好，kaldi形式的ark特征文件
        """
        Path(self.temp_dir).mkdir(parents=True, exist_ok=True)
        feat_txt = self._preprocess_feature(X)

        self._write_model_to_file(self.save_model)
        TEMP_ACC_FILE = self.temp_dir + '/tmp.acc'
        # gmm-global-acc-stats [options] <model-in> <feature-rspecifier> <stats-out>
        run_command([
            'gmm-global-acc-stats',
            self.save_model,
            f'ark:{feat_txt}',
            TEMP_ACC_FILE,
        ], print_stdout=(verbose != 0))

        # global-gmm-adapt-map [options] <model-in> <stats-in> <model-out>
        run_command([
            'global-gmm-adapt-map',
            '--update-flags=mvw',
            f'--mean-tau={tau}',
            self.save_model,
            TEMP_ACC_FILE,
            self.save_model,
        ], print_stdout=(verbose != 0))
        self._read_model_from_file(self.save_model)

        return self

    def predict(self, X, **kwargs):
        print("Warning: use predict_score instand")
        return self.predict_score(X, **kwargs)

    def predict_score(self, X, output_txt=None, average=True, old_api=False, verbose=0):
        Path(self.temp_dir).mkdir(parents=True, exist_ok=True)
        feat_txt = self._preprocess_feature(X)
        if output_txt is None:
            output_txt = self.temp_dir + '/tmp_predict.txt'

        self._write_model_to_file(self.save_model)
        run_command([
            'gmm-global-get-frame-likes',
            '--average={}'.format('true' if average else 'false'),
            self.save_model,
            f'ark:{feat_txt}',
            f'ark,t:{output_txt}',
        ], print_stdout=(verbose != 0))

        # 语法糖：这个逻辑下直接返回结果。
        if average == True:
            if old_api:
                return fetch_averaged_scores(output_txt)
            return np.array(fetch_averaged_scores(output_txt)[1])


class DiagonalGMMBinaryClassifier:
    def __init__(self, model_save_path, num_gauss=512, num_gauss_init_ratio=0.5):
        self.model_save_path = model_save_path
        Path(model_save_path).mkdir(exist_ok=True, parents=True)
        self.pos_gmm = DiagonalGMM(
            os.path.join(model_save_path, 'pos.dgmm'),
            num_gauss=num_gauss,
            num_gauss_init_ratio=num_gauss_init_ratio,
        )
        self.neg_gmm = DiagonalGMM(
            os.path.join(model_save_path, 'neg.dgmm'),
            num_gauss=num_gauss,
            num_gauss_init_ratio=num_gauss_init_ratio,
        )

    def fit(self, X, y, **kwargs):
        X_true = [X[i] for i in range(len(X)) if y[i] == True]
        X_false = [X[i] for i in range(len(X)) if y[i] == False]
        if len(X_true) + len(X_false) != len(X):
            raise ValueError('y must be binary. (just 0 and 1)')
        self.pos_gmm.fit(X_true, **kwargs)
        self.neg_gmm.fit(X_false, **kwargs)
        return self

    def predict_score(self, X):
        pos_scores = self.pos_gmm.predict_score(X)
        neg_scores = self.neg_gmm.predict_score(X)
        log_likelihood_ratio = np.array(pos_scores) - np.array(neg_scores)
        return log_likelihood_ratio


if __name__ == '__main__':
    x = np.random.random((500,100,90))
    model = DiagonalGMM('a.mdl', num_gauss=512)
    model.fit(x, n_jobs=10)

# #%%
#     %prun model.fit(x)
# #%%
#     %lprun -f run_command model.fit(x)
# #%%

