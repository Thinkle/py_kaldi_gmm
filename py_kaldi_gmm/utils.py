from kaldiio import WriteHelper
import subprocess
def run_command(cmd_args, print_stdout=True):
    out = subprocess.Popen(cmd_args,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT,
                           )
    stdout, stderr = out.communicate()

    if out.returncode != 0:
        raise RuntimeError(
            f"Exit {out.returncode};\n"
            f"======= STDOUT&EER =======:\n{stdout.decode('utf-8')}"
            # f"======= STDERR =======:\n{stderr.decode('utf-8')}"
        )
    if print_stdout:
        print(stdout.decode("utf-8"))
    return stdout, stderr

def save_to_kaldi_feature_format(save_file, feat_mats, names=None):
    """ save feature to kaldi format
        Params:
            feat_mats: list of feature shape (t, feature_n)
            names: name of each feature (e.g. the wav name)
            save_file: the path to save
    """
    with WriteHelper(f'ark:{save_file}', compression_method=2) as writer:
        for i in range(len(feat_mats)):
            if names is None:
                name = str(i)
            else:
                name = names[i]
            writer(name, feat_mats[i])